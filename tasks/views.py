from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Task
from .forms import TaskForm


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def user_tasks(request):
    user_task_list = Task.objects.filter(assignee=request.user)
    context = {
        "user_task_list": user_task_list,
    }
    return render(request, "tasks/task_list.html", context)


@login_required
def edit_task(request, id):
    task_edit = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task_edit)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm(instance=task_edit)
    context = {
        "task_object": task_edit,
        "task_form": form,
    }
    return render(request, "tasks/edit_task.html", context)
