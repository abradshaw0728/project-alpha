from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm


# Create your views here.
@login_required
def project_list(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "project_list": list,
    }
    return render(request, "projects/project_list.html", context)


@login_required
def project_details(request, id):
    list = get_object_or_404(Project, id=id)
    context = {
        "task_list": list,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)


@login_required
def edit_project(request, id):
    project_edit = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project_edit)
        if form.is_valid():
            form.save()
            return redirect("show_project", id)
    else:
        form = ProjectForm(instance=project_edit)
    context = {
        "project_object": project_edit,
        "project_form": form,
    }
    return render(request, "projects/edit_project.html", context)
