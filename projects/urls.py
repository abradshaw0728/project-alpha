from django.urls import path
from .views import project_list, project_details, create_project, edit_project


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/edit/", edit_project, name="edit_project"),
]
